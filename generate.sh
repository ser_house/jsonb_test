#!/bin/bash

USER=vagrant
PASSWORD=vagrant
DATABASE=bm

echo "mysql: DROP DATABASE IF EXISTS $DATABASE; CREATE DATABASE $DATABASE;"
mysql -u$USER -p$PASSWORD -e "DROP DATABASE IF EXISTS $DATABASE; CREATE DATABASE $DATABASE;"

echo "mysql: run mysql.sql"
mysql -u$USER -p$PASSWORD $DATABASE < mysql.sql


echo "postgresql: dropdb $DATABASE"
dropdb $DATABASE
echo "postgresql: createdb $DATABASE"
createdb $DATABASE

echo "postgresql: run postgresql_jsonb.sql"
psql -U $USER -d $DATABASE -b -f postgresql_jsonb.sql

echo "postgresql: run postgresql.sql"
psql -U $USER -d $DATABASE -b -f postgresql.sql
