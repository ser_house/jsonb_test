CREATE TABLE product (
  id BIGINT NOT NULL AUTO_INCREMENT,
  memory INT DEFAULT NULL,
  screen_w INT DEFAULT NULL,
  screen_h INT DEFAULT NULL,
  proc VARCHAR(32) DEFAULT NULL,

  PRIMARY KEY (id)
);

CREATE INDEX memory_idx ON product (memory);
CREATE INDEX screen_w_idx ON product (screen_w);
CREATE INDEX screen_h_idx ON product (screen_h);
CREATE INDEX proc_idx ON product (proc);


DELIMITER $$
CREATE PROCEDURE generate_data()
BEGIN
  DECLARE i INT DEFAULT 0;
  WHILE i < 1000000
    DO
      INSERT INTO product (memory, screen_w, screen_h, proc)
      VALUES
        (
          ELT(0.5 + RAND() * 11, 1, 2, 3, 4, 6, 8, 12, 32, 64, 128, 256),
          ELT(0.5 + RAND() * 8, 220, 320, 480, 720, 800, 960, 1080, 1200),
          ELT(0.5 + RAND() * 7, 176, 240, 320, 540, 960, 1600, 1640),
          ELT(0.5 + RAND() * 5, 'A12', 'A13', 'A14', 'A15', 'A16')
          );
      SET i = i + 1;
    END WHILE;
END$$
DELIMITER ;

CALL generate_data();

DROP PROCEDURE generate_data;
