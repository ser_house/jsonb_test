CREATE TABLE product2 (
  id SERIAL PRIMARY KEY,
  memory INT DEFAULT NULL,
  screen_w INT DEFAULT NULL,
  screen_h INT DEFAULT NULL,
  proc VARCHAR(32) DEFAULT NULL
);

CREATE INDEX memory_idx ON product2 USING BTREE (memory);
CREATE INDEX screen_w_idx ON product2 USING BTREE (screen_w);
CREATE INDEX screen_h_idx ON product2 USING BTREE (screen_h);
CREATE INDEX proc_idx ON product2 USING HASH (proc);

-- random_pick

CREATE OR REPLACE FUNCTION random_pick( a anyarray, OUT x anyelement )
  RETURNS anyelement AS
$func$
BEGIN
  IF a = '{}' THEN
    x := NULL::TEXT;
  ELSE
    WHILE x IS NULL LOOP
        x := a[floor(array_lower(a, 1) + (random()*( array_upper(a, 1) -  array_lower(a, 1)+1) ) )::int];
      END LOOP;
  END IF;
END
$func$ LANGUAGE plpgsql VOLATILE RETURNS NULL ON NULL INPUT;

--------------------------------------
DO
$$
  DECLARE
    m  INT;
    w INT;
    h INT;
    p     VARCHAR;
  BEGIN
    FOR r IN 1..1000000
      LOOP

        -- these values are used by many entries
        m := random_pick(ARRAY [1, 2, 3, 4, 6, 8, 12, 32, 64, 128, 256]);
        w := random_pick(ARRAY [220, 320, 480, 720, 800, 960, 1080, 1200]);
        h := random_pick(ARRAY [176, 240, 320, 540, 960, 1600, 1640]);
        p := random_pick(ARRAY ['A12', 'A13', 'A14', 'A15', 'A16']);
        -- JSONB
        INSERT INTO product2 (memory, screen_w, screen_h, proc)
        VALUES (m, w, h, p);

        IF r % 1000 = 0 THEN
          COMMIT;
        END IF;
      END LOOP;
  END
$$;
