CREATE TABLE product (
  id SERIAL PRIMARY KEY,
  data JSONB
);

CREATE INDEX memory_idxb ON product USING BTREE (((data->'memory')::INT));
CREATE INDEX screen_w_idxb ON product USING BTREE (((data->'screen_w')::INT));
CREATE INDEX screen_h_idxb ON product USING BTREE (((data->'screen_h')::INT));
CREATE INDEX proc_idxb ON product USING HASH (((data->'proc')::TEXT));

-- random_pick

CREATE OR REPLACE FUNCTION random_pick( a anyarray, OUT x anyelement )
  RETURNS anyelement AS
$func$
BEGIN
  IF a = '{}' THEN
    x := NULL::TEXT;
  ELSE
    WHILE x IS NULL LOOP
        x := a[floor(array_lower(a, 1) + (random()*( array_upper(a, 1) -  array_lower(a, 1)+1) ) )::int];
      END LOOP;
  END IF;
END
$func$ LANGUAGE plpgsql VOLATILE RETURNS NULL ON NULL INPUT;

--------------------------------------
DO
$$
  DECLARE
    memory  INT;
    screen_w INT;
    screen_h INT;
    proc     VARCHAR;
  BEGIN
    FOR r IN 1..1000000
      LOOP

        -- these values are used by many entries
        memory := random_pick(ARRAY [1, 2, 3, 4, 6, 8, 12, 32, 64, 128, 256]);
        screen_w := random_pick(ARRAY [220, 320, 480, 720, 800, 960, 1080, 1200]);
        screen_h := random_pick(ARRAY [176, 240, 320, 540, 960, 1600, 1640]);
        proc := random_pick(ARRAY ['A12', 'A13', 'A14', 'A15', 'A16']);
        -- JSONB
        INSERT INTO product (data)
        VALUES
          (
            ('{"memory":' || memory || ',"screen_w":' || screen_w || ',"screen_h":' || screen_h || ',"proc": ' || to_json(proc::TEXT) || '}')::JSONB);

        IF r % 1000 = 0 THEN
          COMMIT;
        END IF;
      END LOOP;
  END
$$;
