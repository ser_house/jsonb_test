----------
--- MySQL
----------
SELECT *
FROM product
WHERE memory >= 64
  AND screen_w > 560
  AND proc = 'A15'
ORDER BY memory, screen_w
;

SELECT proc, COUNT(*)
FROM product
GROUP BY proc
ORDER BY proc
;

----------------
--- PostgreSQL
----------------

----------
--- jsonb
----------
SELECT *
FROM product
WHERE (data->'memory')::INT >= 64
  AND (data->'screen_w')::INT > 560
  AND data->>'proc' = 'A15'
ORDER BY (data->'memory')::INT, (data->'screen_w')::INT
;

SELECT data->'proc', COUNT(*)
FROM product
GROUP BY data->'proc'
ORDER BY data->'proc'
;

------------------
--- как и в MySQL
------------------
SELECT *
FROM product2
WHERE memory >= 64
  AND screen_w > 560
  AND proc = 'A15'
ORDER BY memory, screen_w
;

SELECT proc, COUNT(*)
FROM product2
GROUP BY proc
ORDER BY proc
;
